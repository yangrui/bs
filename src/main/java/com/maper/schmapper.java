package com.maper;

import com.beans.sch;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface schmapper {
    @Select("select * from sch where cid=#{cid}")
    public sch selectsch( @Param("cid") int cid);
    @Select("select * from sch")
    public List<sch> selectallsch();
    @Insert("insert into sch values(#{cid},#{cname},#{sc},#{inspc})")
    public int insertsch(sch schi);
    @Update("update sch set cname=#{cname},sc=#{sc},inspc=#{inspc} where cid=#{cid}")
    public int updatesch(sch schu);
    @Delete("delete from sch where cid=#{cid}")
    public int deletesch(@Param("cid") int cid);
}
