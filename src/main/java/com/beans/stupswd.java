package com.beans;

public class stupswd {

    private String s_pswd;

    public stupswd( int sid,String s_pswd) {
        this.s_pswd = s_pswd;
        this.sid = sid;
    }

    private int sid;

    public stupswd() {
    }

    public String getS_pswd() {
        return s_pswd;
    }

    public void setS_pswd(String s_pswd) {
        this.s_pswd = s_pswd;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    @Override
    public String toString() {
        return "stupswd{" +
                "s_pswd='" + s_pswd + '\'' +
                ", sid=" + sid +
                '}';
    }
}
