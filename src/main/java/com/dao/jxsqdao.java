package com.dao;

import com.beans.jxsq;
import com.maper.jxsqmapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

public class jxsqdao {
    private static SqlSessionFactory sqlfactory=sqlinit.getSqlSessionFactory();
    public static jxsq seljxsq(int sid){
        SqlSession session=sqlfactory.openSession();
        jxsqmapper sacmap=session.getMapper(com.maper.jxsqmapper.class);
        jxsq insnum=sacmap.seljxsq(sid);
        session.commit();
        session.close();
        return insnum;
    }
    public static int insjxsq(jxsq saci){
        SqlSession session=sqlfactory.openSession();
        jxsqmapper sacmap=session.getMapper(com.maper.jxsqmapper.class);
        int insnum=sacmap.insjxsq(saci);
        session.commit();
        session.close();
        return insnum;
    }
    public static int updjxsq(int sid,String  jiegou){
        SqlSession session=sqlfactory.openSession();
        jxsqmapper sacmap=session.getMapper(com.maper.jxsqmapper.class);
        int insnum=sacmap.updjxsqjieguo(jiegou,sid);
        session.commit();
        session.close();
        return insnum;
    }
    public static int updjxjb(int sid,String  jxjb){
        SqlSession session=sqlfactory.openSession();
        jxsqmapper sacmap=session.getMapper(com.maper.jxsqmapper.class);
        int insnum=sacmap.updjxsqjxjb(jxjb,sid);
        session.commit();
        session.close();
        return insnum;
    }
    public static List<jxsq> selallsq(){
        SqlSession session=sqlfactory.openSession();
        jxsqmapper sq=session.getMapper(com.maper.jxsqmapper.class);
        List<jxsq> sss=sq.seljxsqall();
        session.commit();
        session.close();
        return sss;
    }
    public static List<jxsq> selnosh(){
        SqlSession session=sqlfactory.openSession();
        jxsqmapper sq=session.getMapper(com.maper.jxsqmapper.class);
        List<jxsq> sss=sq.seljxsqnosh();
        session.commit();
        session.close();
        return sss;
    }
    public static List<jxsq> selcomsh(){
        SqlSession session=sqlfactory.openSession();
        jxsqmapper sq=session.getMapper(com.maper.jxsqmapper.class);
        List<jxsq> sss=sq.seljxsqcomsh();
        session.commit();
        session.close();
        return sss;
    }
    public static void main(String[] args){
      updjxsq(10001,"fasf");
    }

}
