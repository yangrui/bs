package com.dao;
import  com.beans.te;
import com.maper.temapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

public class tedao {
    private static SqlSessionFactory sqlfactory=sqlinit.getSqlSessionFactory();

    public static List<te> selteall(){
        SqlSession session=sqlfactory.openSession();
        temapper temap=session.getMapper(com.maper.temapper.class);
        List<te> telist=temap.selectallte();
        session.commit();
        session.close();
        return telist;
    }
    public static te selteone(int tid){
        SqlSession session=sqlfactory.openSession();
        temapper temap=session.getMapper(com.maper.temapper.class);
        te teone=temap.selectte(tid);
        session.commit();
        session.close();
        return teone;
    }
    public  static int delte(int tid){
        SqlSession session=sqlfactory.openSession();
        temapper temap=session.getMapper(com.maper.temapper.class);
        int delnum=temap.deletete(tid);
        session.commit();
        session.close();
        return delnum;
    }
    public static int updte(te teu){
        SqlSession session=sqlfactory.openSession();
        temapper temap=session.getMapper(com.maper.temapper.class);

        int upnum=temap.updatete(teu);
        session.commit();
        session.close();
        return upnum;
    }
    public static int inste(te tei){
        SqlSession session=sqlfactory.openSession();
        temapper temap=session.getMapper(com.maper.temapper.class);
        int insnum=temap.insertte(tei);
        session.commit();
        session.close();
        return insnum;
    }
}
