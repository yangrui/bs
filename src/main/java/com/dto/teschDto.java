package com.dto;

public class teschDto {
        private String cname;
        private String tads;
        private String ttime;

    public teschDto() {
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getTads() {
        return tads;
    }

    public void setTads(String tads) {
        this.tads = tads;
    }

    public String getTtime() {
        return ttime;
    }

    public void setTtime(String ttime) {
        this.ttime = ttime;
    }

    @Override
    public String toString() {
        return "teschDto{" +
                "cname='" + cname + '\'' +
                ", tads='" + tads + '\'' +
                ", ttime='" + ttime + '\'' +
                '}';
    }
}
