package com.dao;

import com.beans.jx;
import com.maper.jxmapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

public class jxdao {
    private static SqlSessionFactory sqlfactory=sqlinit.getSqlSessionFactory();


    public static List<jx> seljxall(){
        SqlSession session=sqlfactory.openSession();
        jxmapper sacmap=session.getMapper(com.maper.jxmapper.class);
        List<jx> saclist=sacmap.seljx();
        session.commit();
        session.close();
        return saclist;
    }
    public static List<jx> seljxbyid(int sid){
        SqlSession session=sqlfactory.openSession();
        jxmapper sacmap=session.getMapper(com.maper.jxmapper.class);
        List<jx> saclist=sacmap.seljxbyid(sid);
        session.commit();
        session.close();
        return saclist;
    }
    public static int inssac(jx saci){
        SqlSession session=sqlfactory.openSession();
        jxmapper sacmap=session.getMapper(com.maper.jxmapper.class);
        int insnum=sacmap.insjx(saci);
        session.commit();
        session.close();
        return insnum;
    }
}
