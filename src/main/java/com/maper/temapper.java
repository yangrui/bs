package com.maper;

import com.beans.te;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface temapper {
    @Select("select * from te where tid=#{tid}")
    public te selectte(@Param("tid") int tid);
    @Select("select * from te")
    public List<te> selectallte();
    @Insert("insert into te values(#{tid},#{tname},#{ttel},#{tspc})")
    public int insertte(te tei);
    @Update("update te set tname=#{tname},ttel=#{ttel},tspc=#{tspc} where tid=#{tid}")
    public int updatete(te teu);
    @Delete("delete from te where tid=#{tid}")
    public int deletete(@Param("tid") int tid);
}
