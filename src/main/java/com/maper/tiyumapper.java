package com.maper;

import com.beans.tiyu;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface tiyumapper {
    @Select("select * from tiyu where sid=#{sid}")
    List<tiyu> seltiyu(@Param("sid") int sid);
    @Insert("insert into tiyu values(#{sid},#{ytxs},#{fxl},#{tqq},#{ldty},#{wsm},#{yqm},#{fullsc})" )
    int instiyu(tiyu instiyu);
    /*
     private int sid;
    private int ytxs;
    private int fxl;
    private int tqq;
    private int ldty;
    private double wsm;
    private double yqm;
     */

}
