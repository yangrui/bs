package com.maper;

import org.apache.ibatis.annotations.*;
import com.beans.su;

import java.util.List;

public interface sumapper {
    @Select("select * from su where sid=#{sid}")
    public su selectsu( @Param("sid") int sid);
    @Select("select * from su")
    public List<su> selectallsu();
    @Insert("insert into su values(#{sid},#{sname},#{ssex},#{sspc},#{stel})")
    public int insertsu(su sui);
    @Update("update su set sname=#{sname},ssex=#{ssex},sspc=#{sspc},stel=#{stel} where sid=#{sid}")
    public int updatesu(su suu);
    @Delete("delete from su where sid=#{sid}")
    public int deletesu(@Param("sid") int sid);
}
