-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: bs
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jx`
--

DROP TABLE IF EXISTS `jx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `jx` (
  `sid` int(8) DEFAULT NULL,
  `jibie` int(2) DEFAULT NULL,
  `jxm` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jx`
--

LOCK TABLES `jx` WRITE;
/*!40000 ALTER TABLE `jx` DISABLE KEYS */;
INSERT INTO `jx` VALUES (10001,10,'xxx奖'),(10001,2,'xx奖'),(10003,20,'sdas');
/*!40000 ALTER TABLE `jx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jxsq`
--

DROP TABLE IF EXISTS `jxsq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `jxsq` (
  `sid` int(8) unsigned zerofill NOT NULL,
  `allsc` double(4,1) DEFAULT NULL,
  `tystatus` varchar(10) DEFAULT NULL,
  `jxjb` varchar(10) DEFAULT '',
  `jieguo` varchar(100) DEFAULT '',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jxsq`
--

LOCK TABLES `jxsq` WRITE;
/*!40000 ALTER TABLE `jxsq` DISABLE KEYS */;
INSERT INTO `jxsq` VALUES (00010001,99.0,'合格','','拒绝'),(00010002,45.0,'合格','','通过'),(00010003,108.0,'不合格','国家级','通过');
/*!40000 ALTER TABLE `jxsq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sac`
--

DROP TABLE IF EXISTS `sac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sac` (
  `sid` int(8) unsigned zerofill NOT NULL,
  `cid` int(5) unsigned zerofill NOT NULL,
  `asd` varchar(100) DEFAULT ' ',
  `ktime` varchar(100) DEFAULT ' ',
  `mark` int(3) DEFAULT '0',
  `score` float(2,1) DEFAULT '0.0',
  `gpa` float(2,1) DEFAULT '0.0',
  PRIMARY KEY (`sid`,`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sac`
--

LOCK TABLES `sac` WRITE;
/*!40000 ALTER TABLE `sac` DISABLE KEYS */;
INSERT INTO `sac` VALUES (00010001,10003,'asd','2012',54,2.0,0.0),(00010001,12355,'fasf','2018-5-5',88,3.0,4.0),(00010001,54454,'56','55',72,3.0,3.0),(00010002,10002,'dizhi','time',45,5.0,0.0),(00010003,01111,'wu','5-12',88,5.0,4.0);
/*!40000 ALTER TABLE `sac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch`
--

DROP TABLE IF EXISTS `sch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sch` (
  `cid` int(5) unsigned zerofill NOT NULL,
  `cname` varchar(50) NOT NULL,
  `sc` float(2,1) NOT NULL,
  `inspc` varchar(100) NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch`
--

LOCK TABLES `sch` WRITE;
/*!40000 ALTER TABLE `sch` DISABLE KEYS */;
INSERT INTO `sch` VALUES (10002,'计算机概论',5.0,'电子科学与技术'),(10003,'高等数学',5.0,'数学');
/*!40000 ALTER TABLE `sch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `su`
--

DROP TABLE IF EXISTS `su`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `su` (
  `sid` int(8) unsigned zerofill NOT NULL,
  `sname` varchar(100) NOT NULL,
  `ssex` char(2) NOT NULL,
  `sspc` varchar(50) NOT NULL,
  `stel` varchar(50) NOT NULL,
  `sfzid` varchar(30) DEFAULT NULL,
  `stbdate` varchar(30) DEFAULT NULL,
  `bsads` varchar(50) DEFAULT NULL,
  `stldate` varchar(30) DEFAULT NULL,
  `nation` varchar(30) DEFAULT NULL,
  `adds` varchar(30) DEFAULT NULL,
  `fsads` varchar(30) DEFAULT NULL,
  `byxx` varchar(30) DEFAULT NULL,
  `ptbks` varchar(20) DEFAULT NULL,
  `stxz` int(1) DEFAULT NULL,
  `zzmm` varchar(20) DEFAULT NULL,
  `stta` int(3) DEFAULT NULL,
  `stwe` int(3) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `su`
--

LOCK TABLES `su` WRITE;
/*!40000 ALTER TABLE `su` DISABLE KEYS */;
INSERT INTO `su` VALUES (00010001,'学生1','男','信科','183xxxxx','610324xxxxxxxx2319','1995-12-25','xx省xx县','2014-9','汉族','xx省','xx省xx市','xx高中','普通本科生',4,'群众',188,60),(00010002,'学生2','女','专业','150xxxxxxxx','123456789987654321','2018-5-1','xx省xx县','2016-9','xxxx族','xx省','xx省xx市','xx高中','普通本科生',4,'群众',166,60);
/*!40000 ALTER TABLE `su` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `su_pswd`
--

DROP TABLE IF EXISTS `su_pswd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `su_pswd` (
  `sid` int(8) unsigned zerofill NOT NULL,
  `s_pswd` varchar(50) NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `su_pswd`
--

LOCK TABLES `su_pswd` WRITE;
/*!40000 ALTER TABLE `su_pswd` DISABLE KEYS */;
INSERT INTO `su_pswd` VALUES (00010001,'123456'),(00010002,'123456'),(00010003,'123456');
/*!40000 ALTER TABLE `su_pswd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `te`
--

DROP TABLE IF EXISTS `te`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `te` (
  `tid` int(8) unsigned zerofill NOT NULL,
  `tname` varchar(100) NOT NULL,
  `ttel` varchar(50) NOT NULL,
  `tspc` varchar(100) NOT NULL,
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `te`
--

LOCK TABLES `te` WRITE;
/*!40000 ALTER TABLE `te` DISABLE KEYS */;
INSERT INTO `te` VALUES (00054321,'教师2','183123654','数学'),(00123456,'教师1','18312345678','计算机');
/*!40000 ALTER TABLE `te` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `te_pswd`
--

DROP TABLE IF EXISTS `te_pswd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `te_pswd` (
  `tid` int(8) unsigned zerofill NOT NULL,
  `t_pswd` varchar(50) NOT NULL,
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `te_pswd`
--

LOCK TABLES `te_pswd` WRITE;
/*!40000 ALTER TABLE `te_pswd` DISABLE KEYS */;
INSERT INTO `te_pswd` VALUES (00123456,'123456'),(08143111,'18312345678'),(12345678,'12345678');
/*!40000 ALTER TABLE `te_pswd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teach`
--

DROP TABLE IF EXISTS `teach`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `teach` (
  `tid` int(8) unsigned zerofill NOT NULL,
  `cid` int(5) unsigned zerofill NOT NULL,
  `tads` varchar(100) NOT NULL,
  `ttime` varchar(100) NOT NULL,
  PRIMARY KEY (`tid`,`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teach`
--

LOCK TABLES `teach` WRITE;
/*!40000 ALTER TABLE `teach` DISABLE KEYS */;
INSERT INTO `teach` VALUES (00123456,10002,'教一','2:00'),(00123456,10003,'教三','周四 15：00-18：00'),(08143111,12321,'地点6','时间5'),(12345678,12345,'asd','时间');
/*!40000 ALTER TABLE `teach` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiyu`
--

DROP TABLE IF EXISTS `tiyu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tiyu` (
  `sid` int(8) unsigned zerofill NOT NULL,
  `ytxs` int(3) DEFAULT '0',
  `fxl` int(5) DEFAULT '0',
  `tqq` int(3) DEFAULT '0',
  `ldty` int(3) DEFAULT '0',
  `wsm` float(2,1) DEFAULT NULL,
  `yqm` float(4,2) DEFAULT NULL,
  `fullsc` int(3) DEFAULT '0',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiyu`
--

LOCK TABLES `tiyu` WRITE;
/*!40000 ALTER TABLE `tiyu` DISABLE KEYS */;
INSERT INTO `tiyu` VALUES (00000056,5,5,5,5,5.5,5.50,55),(00001002,6,4,4,4,4.2,4.22,55),(00005555,55,55,5,55,5.5,2.20,22),(00010001,6,3902,10,233,7.5,44.22,66),(00010002,7,3333,11,233,7.8,4.44,66),(00010003,6,6666,3,111,8.8,6.66,44),(00010005,4,4444,4,222,6.8,4.22,60),(00010006,7,3600,5,233,7.7,4.44,55);
/*!40000 ALTER TABLE `tiyu` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-16 23:12:16
