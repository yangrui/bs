package com.dao;
import com.beans.sch;
import com.maper.schmapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

public class schdao {
    private static SqlSessionFactory sqlfactory=sqlinit.getSqlSessionFactory();
    /* public static void main(String[] args){
      //selall();
          *//*sch schone=selschone(123);
          System.out.print(schone.toString());*//*
        //  List<sch> schList=selschall();
         sch schup=new sch(123,"计算机概论",5.0,"电子科学与技术");
            try{
                int a =updsch(schup);
                System.out.println(a);
            }catch (Exception e){
                System.out.println("插入失败");
            }



      }*/
    public static List<sch> selschall(){
        SqlSession session=sqlfactory.openSession();
        schmapper schmap=session.getMapper(com.maper.schmapper.class);
        List<sch> schlist=schmap.selectallsch();
        session.commit();
        session.close();
        return schlist;
    }
    public static sch selschone(int cid){
        SqlSession session=sqlfactory.openSession();
        schmapper schmap=session.getMapper(com.maper.schmapper.class);
        sch schone=schmap.selectsch(cid);
        session.commit();
        session.close();
        return schone;
    }
    public  static int delsch(int cid){
        SqlSession session=sqlfactory.openSession();
        schmapper schmap=session.getMapper(com.maper.schmapper.class);
        int delnum=schmap.deletesch(cid);
        session.commit();
        session.close();
        return delnum;
    }
    public static int updsch(sch schu){
        SqlSession session=sqlfactory.openSession();
        schmapper schmap=session.getMapper(com.maper.schmapper.class);

        int upnum=schmap.updatesch(schu);
        session.commit();
        session.close();
        return upnum;
    }
    public static int inssch(sch schi){
        SqlSession session=sqlfactory.openSession();
        schmapper schmap=session.getMapper(com.maper.schmapper.class);
        int insnum=schmap.insertsch(schi);
        session.commit();
        session.close();
        return insnum;
    }
}
