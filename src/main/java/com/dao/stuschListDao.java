package com.dao;

import com.dto.stuDto;
import com.dto.stuschDto;
import com.maper.studentSelectList;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

public class stuschListDao {

  /*  public static  void main(String [] args){
        List<stuschDto> asd=selectStuSch(10000001);
        for(stuschDto s2:asd){
            System.out.println(s2.toString());
        }

    }*/
    private static SqlSessionFactory sqlfactory=sqlinit.getSqlSessionFactory();
/*
    查询课程考试信息
 */
    public static List<stuDto> selstuList(int sid){
        SqlSession session=sqlfactory.openSession();

        studentSelectList stuschmap=session.getMapper(com.maper.studentSelectList.class);
        List<stuDto> studtos=stuschmap.selectSchList(sid);

        session.commit();
        session.close();
        return studtos;
    }
    /*
    查询课程表
     */
    public static List<stuschDto> selectStuSch(int sid){
        SqlSession session=sqlfactory.openSession();

        studentSelectList stuschmap=session.getMapper(com.maper.studentSelectList.class);
        List<stuschDto> stuschdtos=stuschmap.selectStuSch(sid);

        session.commit();
        session.close();
        return stuschdtos;
    }
}
