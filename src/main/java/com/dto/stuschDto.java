package com.dto;
/*
    学生查询课程表
 */
public class stuschDto {
    private String cname;
    private double sc;
    private String inspc;
    private String tads;
    private String ttime;
    private String tname;
    private String ttel;


    public stuschDto(String cname, double sc, String inspc, String tads, String ttime, String tname, String ttel) {
        this.cname = cname;
        this.sc = sc;
        this.inspc = inspc;
        this.tads = tads;
        this.ttime = ttime;
        this.tname = tname;
        this.ttel = ttel;
    }

    public stuschDto() {
    }

    @Override
    public String toString() {
        return "stuschDto{" +
                "cname='" + cname + '\'' +
                ", sc=" + sc +
                ", inspc='" + inspc + '\'' +
                ", tads='" + tads + '\'' +
                ", ttime='" + ttime + '\'' +
                ", tname='" + tname + '\'' +
                ", ttel='" + ttel + '\'' +
                '}';
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public double getSc() {
        return sc;
    }

    public void setSc(double sc) {
        this.sc = sc;
    }

    public String getInspc() {
        return inspc;
    }

    public void setInspc(String inspc) {
        this.inspc = inspc;
    }

    public String getTads() {
        return tads;
    }

    public void setTads(String tads) {
        this.tads = tads;
    }

    public String getTtime() {
        return ttime;
    }

    public void setTtime(String ttime) {
        this.ttime = ttime;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getTtel() {
        return ttel;
    }

    public void setTtel(String ttel) {
        this.ttel = ttel;
    }
}
