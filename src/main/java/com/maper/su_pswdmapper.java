package com.maper;

import com.beans.stupswd;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface su_pswdmapper {
    @Select("select * from su_pswd where sid=#{sid}")
    public stupswd selectsu_pswd(@Param("sid") int sid);
    @Select("select * from su_pswd")
    public List<stupswd> selectallsu_pswd();
    @Insert("insert into su_pswd values(#{sid},#{s_pswd})")
    public int insertsu_pswd(stupswd su_pswdi);
    @Update("update su_pswd set s_pswd=#{s_pswd} where sid=#{sid}")
    public int updatesu_pswd(stupswd su_pswdu);
    @Delete("delete from su_pswd where sid=#{sid}")
    public int deletesu_pswd(@Param("sid") int sid);
}
