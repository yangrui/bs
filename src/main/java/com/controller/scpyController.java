package com.controller;

import com.beans.jx;
import com.beans.jxsq;
import com.beans.tiyu;
import com.dao.jxdao;
import com.dao.jxsqdao;
import com.dao.sacdao;
import com.dao.tiyudao;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class scpyController {
    static boolean flag=false;
    @RequestMapping(value = "/processstatus",method = RequestMethod.GET)
    public String jxsqst(HttpSession session,Model model){

        int sid=(Integer)session.getAttribute("sessionsid");
        List<jxsq> xx= jxsqdao.selallsq();
        if(xx.size()==0){
            /*

             */
            int jiafen= 0;
            double allsc = sacdao.selavg(sid);
            List<jx> jxx=jxdao.seljxbyid(sid);
            //

            for (jx j1:jxx){
                jiafen+=j1.getJibie();

            }
            allsc+=jiafen;
            jxsq jj = new jxsq();
            jj.setSid(sid);
            jj.setAllsc(allsc);
            jj.setJxjb("");
            jj.setJieguo("");
            tiyu tt = new tiyu();
            List<tiyu> t4 = tiyudao.seltiyu(sid);
            for (tiyu yy : t4) {
                tt = yy;
            }
            String tystatus = new String();

            if (tt.getFullsc() < 50) {
                jj.setTystatus("不合格");
                tystatus = "不合格";
            } else {
                jj.setTystatus("合格");
                tystatus = "合格";
            }
            jxsqdao.insjxsq(jj);
            model.addAttribute("allsc",allsc);
            model.addAttribute("tystatus",tystatus);

        }else {

            for(jxsq qwe:xx){
                if(qwe.getSid()==sid){
                    flag=true;
                    System.out.println(qwe.toString());
                    if(qwe.getJxjb().equals("")) {
                        model.addAttribute("allsc", qwe.getAllsc());
                        model.addAttribute("tystatus", qwe.getTystatus());
                        return "sinfo/shurujxsq.html";
                    }
                    else {
                        model.addAttribute("status",qwe);
                        return  "sinfo/selprocess.html";
                    }

                }
                break;
            }
            if(flag==false) {
                double allsc = sacdao.selavg(sid);
                List<jx> jxx=jxdao.seljxbyid(sid);

                int jiafen= 0;
                for (jx j1:jxx){
                    jiafen+=j1.getJibie();

                }
                allsc+=jiafen;
                jxsq jj = new jxsq();
                jj.setSid(sid);
                jj.setAllsc(allsc);
                jj.setJxjb("");
                jj.setJieguo("未审核");
                tiyu tt = new tiyu();
                List<tiyu> t4 = tiyudao.seltiyu(sid);
                for (tiyu yy : t4) {
                    tt = yy;
                }
                String tystatus = new String();
                if (tt.getFullsc() < 60) {
                    jj.setTystatus("不合格");
                    tystatus = "不合格";
                } else {
                    jj.setTystatus("合格");
                    tystatus = "合格";
                }
                jxsqdao.insjxsq(jj);
                model.addAttribute("allsc",allsc);
                model.addAttribute("tystatus",tystatus);
            }

        }

        return "sinfo/shurujxsq.html";

    }
    @RequestMapping(value = "/submitjxsq",method = RequestMethod.GET)
    public String submit(Model model,@Param("jxjb")String jxjb,HttpSession session){
        int sid=(Integer)session.getAttribute("sessionsid");
        int aaa=jxsqdao.updjxjb(sid,jxjb);
        System.out.println(jxjb);
        List<jxsq> xx= jxsqdao.selallsq();
        if(xx.size()==0){
        }else {
            for (jxsq qwe : xx) {
                if (qwe.getSid() == sid) {
                    model.addAttribute("status",qwe);
                    break;
                }
            }
        }
        return "sinfo/selprocess.html";

    }


}
