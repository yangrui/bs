package com.maper;

import com.beans.jx;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface jxmapper {
    @Select("select * from jx")
    List<jx> seljx();
    @Select("select * from jx where sid=#{sid}")
    List<jx> seljxbyid(int sid);
    @Insert("insert into jx values(#{sid},#{jibie},#{jxm})")
    int insjx(jx j1);
}
