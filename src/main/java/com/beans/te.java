package com.beans;

public class te {
    private int tid;
    private  String  tname;
    private  String ttel;
    private  String tspc;

    public te(int tid, String tname, String ttel, String tspc) {
        this.tid = tid;
        this.tname = tname;
        this.ttel = ttel;
        this.tspc = tspc;
    }

    public te() {
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getTtel() {
        return ttel;
    }

    public void setTtel(String ttel) {
        this.ttel = ttel;
    }

    public String getTspc() {
        return tspc;
    }

    public void setTspc(String tspc) {
        this.tspc = tspc;
    }


    @Override
    public String toString() {
        return "te{" +
                "tid=" + tid +
                ", tname='" + tname + '\'' +
                ", ttel='" + ttel + '\'' +
                ", tspc='" + tspc + '\'' +
                '}';
    }
}
