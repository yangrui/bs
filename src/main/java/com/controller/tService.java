package com.controller;

import com.beans.sac;
import com.beans.sch;
import com.beans.tiyu;
import com.dao.sacdao;
import com.dao.schdao;
import com.dao.teschListDao;
import com.dao.tiyudao;
import com.dto.teschDto;
import com.dto.teselscDto;
import com.dto.tete;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class tService {
    /*
    教师查询课表
     */
    @RequestMapping(value="/selecttesch", method = RequestMethod.GET)
    public String teSelSch(Model model, HttpSession session){
        int sessiontid=(Integer)session.getAttribute("sessiontid");

        List<teschDto> tsl=teschListDao.teselschList(sessiontid);
        model.addAttribute("schlist",tsl);
        return "teschList.html";
    }
    /*
   教师查询成绩
    */
    @RequestMapping(value="/selectscore", method = RequestMethod.GET)
    public String teSelScore(Model model,HttpSession session){
        int sessiontid=(Integer) session.getAttribute("sessiontid");
        List<tete> l2=teschListDao.teselscsch(sessiontid);
        model.addAttribute("schlist",l2);
        return "tesc.html";
    }
    /*
    查询成绩为0的选项并准备改成绩
     */
    @RequestMapping(value = "/tealtersc",method = RequestMethod.GET)
    public String altersc(@Param("cname")String cname, Model model,HttpSession session){

        int modelcid= Integer.parseInt(cname);
        session.setAttribute("sessioncid",modelcid);
        int sessiontid=(Integer)session.getAttribute("sessiontid");
        List<teselscDto> scalterlist=teschListDao.tescList(sessiontid,modelcid);

        model.addAttribute("schlist",scalterlist);
        return "altersc.html";
    }
    /*
    tealtersc
     */
    @RequestMapping(value = "/alteronesc",method = RequestMethod.GET)
    public String alteronesc(@Param("sid")String sid,Model model,HttpSession session){
        int sessioncid=(Integer)session.getAttribute("sessioncid");
        int sesid=Integer.parseInt(sid);
        session.setAttribute("tesid",sesid);
        List<sac>   getsac=sacdao.selsacall();
        for(sac s1:getsac){
            if(sesid==s1.getSid()||sessioncid==s1.getCid()){
                model.addAttribute("sid",sesid);

                break;
            }
        }
        return "setonesc.html"; //跳转到输入成绩页面
    }


    /*
    查询已改完的成绩，即不为0的
     */

    @RequestMapping(value = "/comtealtersc",method = RequestMethod.GET)
    public String comaltersc(@Param("mark") String mark, Model model,HttpSession session){

        int setmark= Integer.parseInt(mark);
        int sesid=(Integer)session.getAttribute("tesid");
        int sessioncid=(Integer)session.getAttribute("sessioncid");

        sac stesac=sacdao.selsacone(sesid,sessioncid);
        //根据分数设置学分 绩点
        stesac.setMark(setmark);
        sch setsch= schdao.selschone(sessioncid);
        double a=setsch.getSc();


        if(setmark<60){
            stesac.setGpa(0);
        }
        else if(setmark<70){
            stesac.setGpa(2.0);
            stesac.setScore(a);
        }
        else if(setmark<85){
            stesac.setGpa(3.0);
            stesac.setScore(a);
        }
        else {
            stesac.setGpa(4.0);  stesac.setScore(a);
        }

    //更改
        sacdao.updsac(stesac);



        int sessiontid=(Integer)session.getAttribute("sessiontid");
        List<teselscDto> scalterlist=teschListDao.comtescList(sessiontid,sessioncid);
        for(teselscDto t1:scalterlist){
            System.out.println(t1.toString());
        }
        model.addAttribute("schlist",scalterlist);
        return "comaltersc.html";
    }

    /*

     */
    @RequestMapping(value = "/skaltersc",method = RequestMethod.GET)
    public String skaltersc(){
        return "altersc.html";
    }
    /*

     */
    @RequestMapping(value = "/sktemain",method = RequestMethod.GET)
    public String sktemain(){
        return "teacherMain.html";
    }
    /*
    插入体育测试成绩
     */
    @RequestMapping(value = "/inputtiyu" ,method = RequestMethod.GET)
    public String inputtiyutiyu(){
        return  "inputtiyu.html";
    }
    @RequestMapping(value = "/inserttiyu",method = RequestMethod.GET)
    public String inserttiyu(Model model, @Param("sid")String sid,@Param("ytxs")String ytxs,@Param("fxl")String fxl,@Param("tqq")String tqq,@Param("ldty")String ldty,@Param("wsm")String wsm,@Param("yqm")String yqm,@Param("fullsc")String fullsc){
        int ssid=Integer.parseInt(sid);
        int sytxs=Integer.parseInt(ytxs);
        int sfxl=Integer.parseInt(fxl);
        int stqq=Integer.parseInt(tqq);
        int sldty=Integer.parseInt(ldty);
        double swsm=Double.parseDouble(wsm);
        double syqm=Double.parseDouble(yqm);
        int sfullsc=Integer.parseInt(fullsc);

        tiyu t3=new tiyu();
        t3.setSid(ssid);
        t3.setYtxs(sytxs);
        t3.setFxl(sfxl);
        t3.setTqq(stqq);
        t3.setLdty(sldty);
        t3.setWsm(swsm);
        t3.setYqm(syqm);
        t3.setFullsc(sfullsc);

        try{
            int cginsert= tiyudao.insty(t3);
            tiyu t5=new tiyu();
            List<tiyu> seltiyu=tiyudao.seltiyu(ssid);
            for(tiyu tt:seltiyu){
                t5=tt;
            }
            String message=new String("已成功输入");
            model.addAttribute("ty",t5);
            model.addAttribute("mess",message);

        }catch (Exception e){
            tiyu t5=new tiyu();
            List<tiyu> seltiyu=tiyudao.seltiyu(ssid);
            for(tiyu tt:seltiyu){
                t5=tt;
            }
            String message=new String("学号已存在");
            model.addAttribute("ty",t5);
            model.addAttribute("mess",message);

        }

        return "resulttiyu.html";
    }
    @RequestMapping(value = "/skm",method = RequestMethod.GET)
    public String skm(){
        return "teacherMain.html";
    }

    @RequestMapping(value = "/intiyu" ,method = RequestMethod.GET)
    public String inpuiyutiyu(){
        return  "inputtiyu.html";
    }


}



