package com.beans;

public class su {
    private int sid;
    private String sname;
    private String ssex;
    private String sspc;
    private String stel;
    private String sfzid;
    private String stbdate;
    private String bsads;
    private String stldate;
    private String nation;
    private String adds;
    private String fsads;
    private String byxx;
    private String ptbks;
    private int stxz;
    private String zzmm;
    private int stta;
    private int stwe;

    public su() {
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getSsex() {
        return ssex;
    }

    public void setSsex(String ssex) {
        this.ssex = ssex;
    }

    public String getSspc() {
        return sspc;
    }

    public void setSspc(String sspc) {
        this.sspc = sspc;
    }

    public String getStel() {
        return stel;
    }

    public void setStel(String stel) {
        this.stel = stel;
    }

    public String getSfzid() {
        return sfzid;
    }

    public void setSfzid(String sfzid) {
        this.sfzid = sfzid;
    }

    public String getStbdate() {
        return stbdate;
    }

    public void setStbdate(String stbdate) {
        this.stbdate = stbdate;
    }

    public String getBsads() {
        return bsads;
    }

    public void setBsads(String bsads) {
        this.bsads = bsads;
    }

    public String getStldate() {
        return stldate;
    }

    public void setStldate(String stldate) {
        this.stldate = stldate;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getAdds() {
        return adds;
    }

    public void setAdds(String adds) {
        this.adds = adds;
    }

    public String getFsads() {
        return fsads;
    }

    public void setFsads(String fsads) {
        this.fsads = fsads;
    }

    public String getByxx() {
        return byxx;
    }

    public void setByxx(String byxx) {
        this.byxx = byxx;
    }

    public String getPtbks() {
        return ptbks;
    }

    public void setPtbks(String ptbks) {
        this.ptbks = ptbks;
    }

    public int getStxz() {
        return stxz;
    }

    public void setStxz(int stxz) {
        this.stxz = stxz;
    }

    public String getZzmm() {
        return zzmm;
    }

    public void setZzmm(String zzmm) {
        this.zzmm = zzmm;
    }

    public int getStta() {
        return stta;
    }

    public void setStta(int stta) {
        this.stta = stta;
    }

    public int getStwe() {
        return stwe;
    }

    public void setStwe(int stwe) {
        this.stwe = stwe;
    }

    @Override
    public String toString() {
        return "su{" +
                "sid=" + sid +
                ", sname='" + sname + '\'' +
                ", ssex='" + ssex + '\'' +
                ", sspc='" + sspc + '\'' +
                ", stel='" + stel + '\'' +
                ", sfzid='" + sfzid + '\'' +
                ", stbdate='" + stbdate + '\'' +
                ", bsads='" + bsads + '\'' +
                ", stldate='" + stldate + '\'' +
                ", nation='" + nation + '\'' +
                ", adds='" + adds + '\'' +
                ", fsads='" + fsads + '\'' +
                ", byxx='" + byxx + '\'' +
                ", ptbks='" + ptbks + '\'' +
                ", stxz=" + stxz +
                ", zzmm='" + zzmm + '\'' +
                ", stta=" + stta +
                ", stwe=" + stwe +
                '}';
    }
}
