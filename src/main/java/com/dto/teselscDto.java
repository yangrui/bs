package com.dto;

public class teselscDto {
    private String sname;
    private int sid;
    private String sspc;
    private int mark;

    public teselscDto() {
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getSspc() {
        return sspc;
    }

    public void setSspc(String sspc) {
        this.sspc = sspc;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    @Override
    public String toString() {
        return "teselscDto{" +
                "sname='" + sname + '\'' +
                ", sid=" + sid +
                ", sspc='" + sspc + '\'' +
                ", mark=" + mark +
                '}';
    }
}
