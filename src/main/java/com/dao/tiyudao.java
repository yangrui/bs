package com.dao;

import com.beans.tiyu;
import com.maper.tiyumapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

public class tiyudao {
    private static SqlSessionFactory sqlfactory=sqlinit.getSqlSessionFactory();

    public static List<tiyu> seltiyu(int sid){
        SqlSession session=sqlfactory.openSession();
        tiyumapper temap=session.getMapper(com.maper.tiyumapper.class);
        List<tiyu> telist=temap.seltiyu(sid);
        session.commit();
        session.close();
        return telist;
    }
    public static int insty(tiyu tei){
        SqlSession session=sqlfactory.openSession();
        tiyumapper temap=session.getMapper(com.maper.tiyumapper.class);
        int insnum=temap.instiyu(tei);
        session.commit();
        session.close();
        return insnum;
    }


}
