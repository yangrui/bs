package com.dto;

public class tete {
    private String cname;
    private int cid;

    public tete() {
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    @Override
    public String toString() {
        return "tete{" +
                "cname='" + cname + '\'' +
                ", cid=" + cid +
                '}';
    }
}
