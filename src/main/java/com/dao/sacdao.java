package com.dao;

import com.beans.sac;
import com.maper.sacmapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

public class sacdao {
    private static SqlSessionFactory sqlfactory=sqlinit.getSqlSessionFactory();

    public static List<sac> selsacall(){
        SqlSession session=sqlfactory.openSession();
        sacmapper sacmap=session.getMapper(com.maper.sacmapper.class);
        List<sac> saclist=sacmap.selectallsac();
        session.commit();
        session.close();
        return saclist;
    }
    public static sac selsacone(int sid,int cid){
        SqlSession session=sqlfactory.openSession();
        sacmapper sacmap=session.getMapper(com.maper.sacmapper.class);
        sac sacone=sacmap.selectsac(sid,cid);
        session.commit();
        session.close();
        return sacone;
    }
    public  static int delsac(int sid,int cid){
        SqlSession session=sqlfactory.openSession();
        sacmapper sacmap=session.getMapper(com.maper.sacmapper.class);
        int delnum=sacmap.deletesac(sid,cid);
        session.commit();
        session.close();
        return delnum;
    }
    public static int updsac(sac sacu){
        SqlSession session=sqlfactory.openSession();
        sacmapper sacmap=session.getMapper(com.maper.sacmapper.class);

        int upnum=sacmap.updatesac(sacu);
        session.commit();
        session.close();
        return upnum;
    }
    public static int inssac(sac saci){
        SqlSession session=sqlfactory.openSession();
        sacmapper sacmap=session.getMapper(com.maper.sacmapper.class);
        int insnum=sacmap.insertsac(saci);
        session.commit();
        session.close();
        return insnum;
    }
    public  static double selavg(int sid){
        SqlSession session=sqlfactory.openSession();
        sacmapper sacmap=session.getMapper(com.maper.sacmapper.class);
        double delnum=sacmap.selavg(sid);
        session.commit();
        session.close();
        return delnum;
    }

    /*

     */

}
