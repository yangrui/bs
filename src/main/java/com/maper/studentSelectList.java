package com.maper;

import com.dto.stuDto;
import com.dto.stuschDto;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface studentSelectList {
    @Select( "select sch.cname,sch.sc,sch.inspc,sac.asd,sac.ktime,sac.mark,sac.score,sac.gpa from sac,sch where sac.sid=#{sid} and sac.cid=sch.cid")
     List<stuDto> selectSchList(@Param("sid") int sid);
    @Select("select sch.cname,sch.sc,sch.inspc,teach.tads,teach.ttime,te.tname,te.ttel from sac,sch,teach,te where sac.sid=#{sid} and sac.cid=teach.cid and sac.cid=sch.cid and teach.tid=te.tid")
     List<stuschDto> selectStuSch(@Param("sid") int sid);

}
