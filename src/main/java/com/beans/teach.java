package com.beans;

public class teach {
    private int tid;
    private int cid;
    private String tads;

    public teach(int tid, int cid, String tads, String ttime) {
        this.tid = tid;
        this.cid = cid;
        this.tads = tads;
        this.ttime = ttime;
    }

    public teach() {
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getTads() {
        return tads;
    }

    public void setTads(String tads) {
        this.tads = tads;
    }

    public String getTtime() {
        return ttime;
    }

    public void setTtime(String ttime) {
        this.ttime = ttime;
    }

    private String ttime;

    @Override
    public String toString() {
        return "teach{" +
                "tid=" + tid +
                ", cid=" + cid +
                ", tads='" + tads + '\'' +
                ", ttime='" + ttime + '\'' +
                '}';
    }
}
