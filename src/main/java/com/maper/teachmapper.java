package com.maper;

import com.beans.teach;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface teachmapper {
    @Select("select * from teach where tid=#{tid} and cid=#{cid}")
    public teach selectteach(@Param("tid") int tid, @Param("cid") int cid);
    @Select("select * from teach")
    public List<teach> selectallteach();
    @Insert("insert into teach values(#{tid},#{cid},#{tads},#{ttime})")
    public int insertteach(teach teachi);
    @Update("update teach set tads=#{tads},ttime=#{ttime} where tid=#{tid} and cid=#{cid}")
    public int updateteach(teach teachu);
    @Delete("delete from teach where tid=#{tid} and cid=#{cid}")
    public int deleteteach(@Param("tid") int tid,@Param("cid") int cid);
}
