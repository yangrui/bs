package com.maper;

import com.beans.sac;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface sacmapper {
    @Select("select * from sac where sid=#{sid} and cid=#{cid}")
    public sac selectsac(@Param("sid") int sid, @Param("cid") int cid);
    @Select("select * from sac")
    public List<sac> selectallsac();
    @Insert("insert into sac values(#{sid},#{cid},#{asd},#{ktime},#{mark},#{score},#{gpa})")
    public int insertsac(sac saci);
    @Update("update sac set asd=#{asd},ktime=#{ktime},mark=#{mark},score=#{score},gpa=#{gpa} where sid=#{sid} and cid=#{cid}")
    public int updatesac(sac sacu);
    @Delete("delete from sac where sid=#{sid} and cid=#{cid}")
    public int deletesac(@Param("sid") int sid,@Param("cid") int cid);
    /*
    查询平均成绩
     */
    @Select("select avg(mark) from sac where sid=#{sid}")
    double selavg(int sid);
}
