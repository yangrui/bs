package com.dto;
/*
    学生考试情况查询
 */
public class stuDto {
    private String cname;
    private double sc;
    private String inspc;
    private String asd;
    private String ktime;
    private int mark;
    private double score;
    private double gpa;

    public stuDto(String cname, double sc, String inspc, String asd, String ktime, int mark, double score, double gpa) {
        this.cname = cname;
        this.sc = sc;
        this.inspc = inspc;
        this.asd = asd;
        this.ktime = ktime;
        this.mark = mark;
        this.score = score;
        this.gpa = gpa;
    }

    public stuDto() {
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public double getSc() {
        return sc;
    }

    public void setSc(double sc) {
        this.sc = sc;
    }

    public String getInspc() {
        return inspc;
    }

    public void setInspc(String inspc) {
        this.inspc = inspc;
    }

    public String getAsd() {
        return asd;
    }

    public void setAsd(String asd) {
        this.asd = asd;
    }

    public String getKtime() {
        return ktime;
    }

    public void setKtime(String ktime) {
        this.ktime = ktime;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }

    @Override
    public String toString() {
        return "stuDto{" +
                "cname='" + cname + '\'' +
                ", sc=" + sc +
                ", inspc='" + inspc + '\'' +
                ", asd='" + asd + '\'' +
                ", ktime='" + ktime + '\'' +
                ", mark=" + mark +
                ", score=" + score +
                ", gpa=" + gpa +
                '}';
    }
}
