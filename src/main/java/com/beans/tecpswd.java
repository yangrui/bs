package com.beans;

public class tecpswd {

    private int tid;
    private  String t_pswd;

    public tecpswd(int tid, String t_pswd) {
        this.tid = tid;
        this.t_pswd = t_pswd;
    }

    public tecpswd() {
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public String getT_pswd() {
        return t_pswd;
    }

    public void setT_pswd(String t_pswd) {
        this.t_pswd = t_pswd;
    }


    @Override
    public String toString() {
        return "tecpswd{" +

                "tid=" + tid +", t_pswd='" + t_pswd  +  '\''+
                '}';
    }
}
