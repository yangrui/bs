package com.beans;
public class sac {
    private int sid;
    private int cid;
    private String asd;
    private String ktime;
    private int mark;
    private double score;
    private double gpa;

    public sac(int sid, int cid, String asd, String ktime, int mark, double score, double gpa) {
        this.sid = sid;
        this.cid = cid;
        this.asd = asd;
        this.ktime = ktime;
        this.mark = mark;
        this.score = score;
        this.gpa = gpa;
    }

    public sac() {
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getAsd() {
        return asd;
    }

    public void setAsd(String asd) {
        this.asd = asd;
    }

    public String getKtime() {
        return ktime;
    }

    public void setKtime(String ktime) {
        this.ktime = ktime;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }


    @Override
    public String toString() {
        return "sac{" +
                "sid=" + sid +
                ", cid=" + cid +
                ", asd='" + asd + '\'' +
                ", ktime='" + ktime + '\'' +
                ", mark=" + mark +
                ", score=" + score +
                ", gpa=" + gpa +
                '}';
    }
}
