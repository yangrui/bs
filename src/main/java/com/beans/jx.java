package com.beans;

public class jx {
    private  int sid;
    private  int jibie;
    private  String jxm;

    public jx() {
    }

    @Override
    public String toString() {
        return "jx{" +
                "sid=" + sid +
                ", jibie=" + jibie +
                ", jxm='" + jxm + '\'' +
                '}';
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getJibie() {
        return jibie;
    }

    public void setJibie(int jibie) {
        this.jibie = jibie;
    }

    public String getJxm() {
        return jxm;
    }

    public void setJxm(String jxm) {
        this.jxm = jxm;
    }
}
