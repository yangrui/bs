package com.dao;

import com.beans.teach;
import com.maper.teachmapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

public class teachdao {
    private static SqlSessionFactory sqlfactory=sqlinit.getSqlSessionFactory();

    /* public static void main(String[] args){
      //selone;
          *//*teach teone=selteachone(12345678,12345);
          System.out.print(teone.toString());*//*

    //selall
        *//*  List<teach> teList=selteachall();
          for(teach te1:teList){
              System.out.println(te1.toString());
          }*//*

    // insert
       *//* teach teup=new teach(8143111,12321,"地点2","时间3");

            int a =insteach(teup);
            System.out.println(a);*//*

    //delete
        *//*int a=delteach(12345679,12346);
        System.out.println(a);*//*

    //update
        teach teup=new teach(8143111,12321,"地点6","时间5");
        int a=updteach(teup);
        System.out.println(a);

     }*/
    public static List<teach> selteachall(){
        SqlSession session=sqlfactory.openSession();
        teachmapper teachmap=session.getMapper(com.maper.teachmapper.class);
        List<teach> teachlist=teachmap.selectallteach();
        session.commit();
        session.close();
        return teachlist;
    }
    public static teach selteachone(int tid,int cid){
        SqlSession session=sqlfactory.openSession();
        teachmapper teachmap=session.getMapper(com.maper.teachmapper.class);
        teach teachone=teachmap.selectteach(tid,cid);
        session.commit();
        session.close();
        return teachone;
    }
    public  static int delteach(int tid,int cid){
        SqlSession session=sqlfactory.openSession();
        teachmapper teachmap=session.getMapper(com.maper.teachmapper.class);
        try{
            int delnum=teachmap.deleteteach(tid,cid);
            session.commit();
            return delnum;
        }catch (Exception e){
            return 2;
        }
        finally {
            session.close();
        }


    }
    public static int updteach(teach teachu){
        SqlSession session=sqlfactory.openSession();
        teachmapper teachmap=session.getMapper(com.maper.teachmapper.class);

            int upnum=teachmap.updateteach(teachu);


           session.commit();

            session.close();
        return upnum;


    }
    public static int insteach(teach teachi){
        SqlSession session=sqlfactory.openSession();
        teachmapper teachmap=session.getMapper(com.maper.teachmapper.class);
        int insnum=teachmap.insertteach(teachi);
        session.commit();
        session.close();
        return insnum;
    }
}
