package com.controller;

import com.beans.stupswd;
import com.beans.su;
import com.beans.tecpswd;
import com.dao.su_pswddao;
import com.dao.sudao;
import com.dao.te_pswddao;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.LinkedList;
import java.util.List;

@Controller
public class loginController {

    /*
       学生登录界面
    */
    @RequestMapping(value = "/studentlogin",method = RequestMethod.POST)
    public String stuLogin(@RequestParam("sid") String sid, @RequestParam("pswd") String pswd, Model model, HttpSession session){

        try {
            int a = Integer.parseInt(sid);

            List<stupswd> s2 = new LinkedList<stupswd>();
            s2 = su_pswddao.selstupswdall();
            boolean flag = false;
            String b = new String();
            if (s2.size() == 0) {
                model.addAttribute("errorMessage", "用户名不存在");

            } else {
                for (stupswd s3 : s2) {
                    if (s3.getSid() == a) {

                        flag = true;
                        b = s3.getS_pswd();
                        if(pswd.equals(b)){
                            //登录成功....
                            session.setAttribute("sessionsid",a);

                            return "studentMain.html";
                        }else {
                            model.addAttribute("errorMessage", "密码错误");


                        }
                        break;
                    }else {
                    }
                }
                if(flag==false) {
                    model.addAttribute("errorMessage", "用户名不存在");

                }
            }
        }catch (Exception e){
            model.addAttribute("errorMessage", "用户名错误");

        }
        return "stuloginerr.html";
    }
    /*
    教师登录
     */
    @RequestMapping(value="/skipteacherlogin",method = RequestMethod.GET)
    public String skipteachlogin(){
        return "teacherlogin.html";
    }
    @RequestMapping(value="/teacherlogin",method = RequestMethod.GET)
    public String tecLogin(@RequestParam("tid") String tid, @RequestParam("pswd") String pswd, Model model,HttpSession session){
        try {
            int a = Integer.parseInt(tid);

            List<tecpswd> s2 = te_pswddao.seltecpswdall();
            boolean flag = false;
            String b = new String();
            if (s2.size() == 0) {
                model.addAttribute("errorMessage", "用户名不存在");

            } else {
                for (tecpswd s3 : s2) {
                    if (s3.getTid() == a) {

                        flag = true;
                        b = s3.getT_pswd();
                        if(pswd.equals(b)){
                            //登录成功....
                            session.setAttribute("sessiontid",a);


                            return "teacherMain.html";
                        }else {
                            model.addAttribute("errorMessage", "密码错误");


                        }
                        break;
                    }else {
                    }
                }
                if(flag==false) {
                    model.addAttribute("errorMessage", "用户名不存在");

                }
            }
        }catch (Exception e){
            model.addAttribute("errorMessage", "用户名错误");

        }
        return "tealoginerr.html";
    }
    @RequestMapping(value = "/tearelogin")
    /*

     */
    public String tearelogin(){return "teacherlogin.html";};
}
