package com.beans;

public class sch {
    private int cid;
    private String cname;
    private double sc;
    private String inspc;

    public sch(int cid, String cname, double sc, String inspc) {
        this.cid = cid;
        this.cname = cname;
        this.sc = sc;
        this.inspc = inspc;
    }

    public sch() {
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public double getSc() {
        return sc;
    }

    public void setSc(double sc) {
        this.sc = sc;
    }

    public String getInspc() {
        return inspc;
    }

    public void setInspc(String inspc) {
        this.inspc = inspc;
    }


    @Override
    public String toString() {
        return "sch{" +
                "cid=" + cid +
                ", cname='" + cname + '\'' +
                ", sc=" + sc +
                ", inspc='" + inspc + '\'' +
                '}';
    }
}
