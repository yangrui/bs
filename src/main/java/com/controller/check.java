package com.controller;

import com.beans.jxsq;
import com.dao.jxsqdao;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class check {
    @RequestMapping(value="/check",method=RequestMethod.GET)
    public String  check(Model model){
        List<jxsq> sqlist= jxsqdao.selallsq();
        model.addAttribute("noshlist",sqlist);
        return "check/noshcat.html";
    }
    @RequestMapping(value = "/tijiaosid",method = RequestMethod.GET)
    public String tijiaosid(@Param("sid")int sid,Model model,HttpSession session){
       // int sid=Integer.parseInt(strsid);

        jxsq jjsq=jxsqdao.seljxsq(sid);
        model.addAttribute("jjj",jjsq);
        session.setAttribute("checksid",sid);
        return "check/req.html";
    }

    @RequestMapping(value = "/yes",method = RequestMethod.GET)
    public String yes(HttpSession session,Model model){

        int sid=(Integer)session.getAttribute("checksid");
        int res=jxsqdao.updjxsq(sid,"通过");

        List<jxsq> sqlist= jxsqdao.selallsq();
        model.addAttribute("noshlist",sqlist);
        return "check/noshcat.html";


    }
    @RequestMapping(value = "/no",method = RequestMethod.GET)
    public String no(HttpSession session,Model model){

        int sid=(Integer)session.getAttribute("checksid");
        int res=jxsqdao.updjxsq(sid,"拒绝");

        List<jxsq> sqlist= jxsqdao.selallsq();
        model.addAttribute("noshlist",sqlist);
        return "check/noshcat.html";


    }
}
