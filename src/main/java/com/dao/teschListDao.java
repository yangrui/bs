package com.dao;

import com.dto.stuDto;
import com.dto.teschDto;
import com.dto.teselscDto;
import com.dto.tete;
import com.maper.studentSelectList;
import com.maper.teServiceMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

public class teschListDao {
    private static SqlSessionFactory sqlfactory=sqlinit.getSqlSessionFactory();
    /*
        教师查询课表信息
     */
   /* public static void main(String [] args){
        List<teselscDto> s3=tescList(123456,10002);
        for(teselscDto t1:s3){
            System.out.println(t1.toString());
        }
    }*/

    public static List<teschDto> teselschList(int tid){
        SqlSession session=sqlfactory.openSession();

        teServiceMapper stuschmap=session.getMapper(com.maper.teServiceMapper.class);
        List<teschDto> studtos=stuschmap.teselschList(tid);

        session.commit();
        session.close();
        return studtos;
    }
    /*
       查询教师所教课程
     */
    public static List<tete> teselscsch(int tid){
        SqlSession session=sqlfactory.openSession();

        teServiceMapper stuschmap=session.getMapper(com.maper.teServiceMapper.class);
        List<tete> studtos=stuschmap.teselscsch(tid);

        session.commit();
        session.close();
        return studtos;
    }
    /*
    查询未改成绩
     */
    public static List<teselscDto> tescList(int tid,int cid){
        SqlSession session=sqlfactory.openSession();

        teServiceMapper stuschmap=session.getMapper(com.maper.teServiceMapper.class);
        List<teselscDto> studtos=stuschmap.tescList(tid,cid);

        session.commit();
        session.close();
        return studtos;

    }
    /*
    查询已改成绩
     */
    public static List<teselscDto> comtescList(int tid,int cid){
        SqlSession session=sqlfactory.openSession();

        teServiceMapper stuschmap=session.getMapper(com.maper.teServiceMapper.class);
        List<teselscDto> studtos=stuschmap.comtescList(tid,cid);
        session.commit();
        session.close();
        return studtos;

    }
}
