package com.dao;

import com.beans.tecpswd;
import com.maper.te_pswdmapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

public class te_pswddao {
    private static SqlSessionFactory sqlfactory=sqlinit.getSqlSessionFactory();
    /*public static void main(String[] args){
        //selone;
          tecpswd suone=seltecpswdone(8143111);
          System.out.print(suone.toString());
        //selall
        *//*  List<tecpswd> suList=seltecpswdall();
          for(tecpswd su1:suList){
              System.out.println(su1.toString());
          }*//*

        // insert
       *//* tecpswd suup=new tecpswd(8143111,"18312345678");

            int a =instecpswd(suup);
            System.out.println(a);*//*

        //delete
        *//*int a=deltecpswd(8143114);
        System.out.println(a);*//*

        //update
       *//* tecpswd suup=new tecpswd(8143114,"1878");
        int a=updtecpswd(suup);
        System.out.println(a);*//*



    }*/
    public static List<tecpswd> seltecpswdall(){
        SqlSession session=sqlfactory.openSession();
        te_pswdmapper tecpswdmap=session.getMapper(com.maper.te_pswdmapper.class);
        List<tecpswd> tecpswdlist=tecpswdmap.selectallte_pswd();
        session.commit();
        session.close();
        return tecpswdlist;
    }
    public static tecpswd seltecpswdone(int tid){
        SqlSession session=sqlfactory.openSession();
        te_pswdmapper tecpswdmap=session.getMapper(com.maper.te_pswdmapper.class);
        tecpswd tecpswdone=tecpswdmap.selectte_pswd(tid);
        session.commit();
        session.close();
        return tecpswdone;
    }
    public  static int deltecpswd(int tid){
        SqlSession session=sqlfactory.openSession();
        te_pswdmapper tecpswdmap=session.getMapper(com.maper.te_pswdmapper.class);
        int delnum=tecpswdmap.deletete_pswd(tid);
        session.commit();
        session.close();
        return delnum;
    }
    public static int updtecpswd(tecpswd tecpswdu){
        SqlSession session=sqlfactory.openSession();
        te_pswdmapper tecpswdmap=session.getMapper(com.maper.te_pswdmapper.class);

        int upnum=tecpswdmap.updatete_pswd(tecpswdu);
        session.commit();
        session.close();
        return upnum;
    }
    public static int instecpswd(tecpswd tecpswdi){
        SqlSession session=sqlfactory.openSession();
        te_pswdmapper tecpswdmap=session.getMapper(com.maper.te_pswdmapper.class);
        int insnum=tecpswdmap.insertte_pswd(tecpswdi);
        session.commit();
        session.close();
        return insnum;
    }
}
