package com.maper;

import com.beans.jxsq;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface jxsqmapper {
    @Select("select * from jxsq where sid=#{sid}")
    jxsq seljxsq(@Param("sid") int sid);

    @Select("select * from jxsq")
    List<jxsq> seljxsqall();

    @Select("select * from jxsq where jieguo=\"\" ")
    List<jxsq> seljxsqnosh();
    @Select("select * from jxsq where jieduo!=\"\" ")
    List<jxsq> seljxsqcomsh();


    @Insert("insert into jxsq values(#{sid},#{allsc},#{tystatus},#{jxjb},#{jieguo})")
    int insjxsq(jxsq q1);

    @Update("update jxsq set jieguo=#{jieguo} where sid=#{sid}")
    int updjxsqjieguo(@Param("jieguo") String jiegou,@Param("sid") int sid);

    @Update("update jxsq set jxjb=#{jxjb} where sid=#{sid}")
    int updjxsqjxjb(@Param("jxjb") String jxjb,@Param("sid") int sid);
}
