package com.beans;

public class jxsq {
    private int sid;
    private double allsc;
    private String tystatus;
    private  String jxjb;
    private  String jieguo;

    @Override
    public String toString() {
        return "jxsq{" +
                "sid=" + sid +
                ", allsc=" + allsc +
                ", tystatus='" + tystatus + '\'' +
                ", jxjb='" + jxjb + '\'' +
                ", jieguo='" + jieguo + '\'' +
                '}';
    }

    public jxsq() {
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public double getAllsc() {
        return allsc;
    }

    public void setAllsc(double allsc) {
        this.allsc = allsc;
    }

    public String getTystatus() {
        return tystatus;
    }

    public void setTystatus(String tystatus) {
        this.tystatus = tystatus;
    }

    public String getJxjb() {
        return jxjb;
    }

    public void setJxjb(String jxjb) {
        this.jxjb = jxjb;
    }

    public String getJieguo() {
        return jieguo;
    }

    public void setJieguo(String jieguo) {
        this.jieguo = jieguo;
    }
}
