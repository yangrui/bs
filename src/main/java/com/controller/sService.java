package com.controller;

import com.beans.jx;
import com.beans.su;
import com.beans.tiyu;
import com.dao.jxdao;
import com.dao.stuschListDao;
import com.dao.sudao;
import com.dao.tiyudao;
import com.dto.stuDto;
import com.dto.stuschDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.LinkedList;
import java.util.List;

@Controller
public class sService {
    /*
        查询课表
     */
    @RequestMapping(value="/selectsch",method=RequestMethod.GET)
    public String stuselsch(Model model, HttpSession session){
        int sessionsid=(Integer)session.getAttribute("sessionsid");
        List<stuschDto> schlist=stuschListDao.selectStuSch(sessionsid);


        model.addAttribute("schlist",schlist);

        return "stuschList.html";
    }
    /*
    查询体育测试信息
     */
    @RequestMapping(value="/tiyuinfo",method=RequestMethod.GET)
    public String seltiyu(Model model,HttpSession session){
        int sid=(Integer)session.getAttribute("sessionsid");
        tiyu t1=new tiyu();
        String mess=new String();
        List<tiyu> sty= tiyudao.seltiyu(sid);
        if(sty.size()!=0){
            for(tiyu tt:sty){
                t1=tt;
            }
            if(t1.getFullsc()<60){
               mess="不合格";
            }else {
                mess="合格";
            }
            model.addAttribute("mess",mess);
            model.addAttribute("ty",t1);
            return "tiyuinfo.html";
        }
        else {

        }
        return "tiyuinfo.html";
    }
    /*
    查询考试
     */
    @RequestMapping(value="/selectexm",method=RequestMethod.GET)
    public String stuselexm(Model model, HttpSession session){

        int sessionsid=(Integer)session.getAttribute("sessionsid");
        List<stuDto> schlist=stuschListDao.selstuList(sessionsid);


        model.addAttribute("schlist",schlist);
        return "studentexm.html";
    }
    /*
    跳转到学生主页
     */
    @RequestMapping(value = "/skipstumain",method=RequestMethod.GET)
    public String skipmain(){
        return "studentMain.html";
    }
    /*
    查询基础信息
     */
    @RequestMapping(value = "/selbaseinfo",method = RequestMethod.GET)
    public String selbaseinfo(Model model,HttpSession session){
        int sesseionsid=(Integer)session.getAttribute("sessionsid");
        su ss1=sudao.selsuone(sesseionsid);
        List<su> s1=new LinkedList<su>();
        ((LinkedList<su>) s1).addFirst(ss1);

        System.out.println(s1.toString());
        model.addAttribute("suu",s1);
        return "stbaseinfo.html";
    }
    /*
    跳转到考级报名
     */
    @RequestMapping(value = "/kjbm",method = RequestMethod.GET)
    public String kjbm(){
        return "kjbm.html";
    }
    /*
    获奖查询
     */
    @RequestMapping(value = "/jxinfo",method = RequestMethod.GET)
    public String jxinfo(HttpSession session,Model model){
        int sssid =(Integer)session.getAttribute("sessionsid");

        List<jx> jj= jxdao.seljxbyid(sssid);

        model.addAttribute("jxlist",jj);

        return "sinfo/jxinfo.html";
    }

}
