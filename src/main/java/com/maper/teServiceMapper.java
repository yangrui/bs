package com.maper;

import com.dto.teschDto;
import com.dto.teselscDto;
import com.dto.tete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface teServiceMapper {
    @Select("select sch.cname,teach.tads,teach.ttime from teach,sch where tid=#{tid} and teach.cid=sch.cid")
    public List<teschDto> teselschList(@Param("tid") int tid);
    @Select("select sch.cname,sch.cid from teach,sch where teach.tid=#{tid} and teach.cid=sch.cid")
    public List<tete> teselscsch(@Param("tid") int tid);
    @Select("select su.sname,su.sid,su.sspc,sac.mark from teach,su,sac where teach.tid=#{tid} and teach.cid=#{cid} and teach.cid=sac.cid and sac.sid=su.sid and sac.mark=0")
    public List<teselscDto> tescList(@Param("tid") int tid,@Param("cid") int cid);
    @Select("select su.sname,su.sid,su.sspc,sac.mark from teach,su,sac where teach.tid=#{tid} and teach.cid=#{cid} and teach.cid=sac.cid and sac.sid=su.sid and sac.mark!=0")
    public List<teselscDto> comtescList(@Param("tid") int tid,@Param("cid") int cid);
}
