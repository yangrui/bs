package com.maper;

import com.beans.tecpswd;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface te_pswdmapper {
    @Select("select * from te_pswd where tid=#{tid}")
    public tecpswd selectte_pswd(@Param("tid") int tid);
    @Select("select * from te_pswd")
    public List<tecpswd> selectallte_pswd();
    @Insert("insert into te_pswd values(#{tid},#{t_pswd})")
    public int insertte_pswd(tecpswd te_pswdi);
    @Update("update te_pswd set t_pswd=#{t_pswd} where tid=#{tid}")
    public int updatete_pswd(tecpswd te_pswdu);
    @Delete("delete from te_pswd where tid=#{tid}")
    public int deletete_pswd(@Param("tid") int tid);
}
