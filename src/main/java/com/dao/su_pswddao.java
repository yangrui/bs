package com.dao;

import com.beans.stupswd;
import com.maper.su_pswdmapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

public class su_pswddao {
    private static SqlSessionFactory sqlfactory=sqlinit.getSqlSessionFactory();
    /*public static void main(String[] args){
        //selone;
         *//* stupswd suone=selstupswdone(8143114);
          System.out.print(suone.toString());
*//*
        //selall
         *//* List<stupswd> suList=selstupswdall();
          for(stupswd su1:suList){
              System.out.println(su1.toString());
          }*//*

        // insert
       *//* stupswd suup=new stupswd(8143111,"18312345678");

            int a =insstupswd(suup);
            System.out.println(a);*//*

        //delete
        *//*int a=delstupswd(8143114);
        System.out.println(a);*//*

        //update
        stupswd suup=new stupswd(8143111,"1878");
        int a=updstupswd(suup);
        System.out.println(a);



    }*/
    public static List<stupswd> selstupswdall(){
        SqlSession session=sqlfactory.openSession();
        su_pswdmapper stupswdmap=session.getMapper(com.maper.su_pswdmapper.class);
        List<stupswd> stupswdlist=stupswdmap.selectallsu_pswd();
        session.commit();
        session.close();
        return stupswdlist;
    }
    public static stupswd selstupswdone(int sid){
        SqlSession session=sqlfactory.openSession();
        su_pswdmapper stupswdmap=session.getMapper(com.maper.su_pswdmapper.class);
        stupswd stupswdone=stupswdmap.selectsu_pswd(sid);
        session.commit();
        session.close();
        return stupswdone;
    }
    public  static int delstupswd(int sid){
        SqlSession session=sqlfactory.openSession();
        su_pswdmapper stupswdmap=session.getMapper(com.maper.su_pswdmapper.class);
        int delnum=stupswdmap.deletesu_pswd(sid);
        session.commit();
        session.close();
        return delnum;
    }
    public static int updstupswd(stupswd stupswdu){
        SqlSession session=sqlfactory.openSession();
        su_pswdmapper stupswdmap=session.getMapper(com.maper.su_pswdmapper.class);

        int upnum=stupswdmap.updatesu_pswd(stupswdu);
        session.commit();
        session.close();
        return upnum;
    }
    public static int insstupswd(stupswd stupswdi){
        SqlSession session=sqlfactory.openSession();
        su_pswdmapper stupswdmap=session.getMapper(com.maper.su_pswdmapper.class);
        int insnum=stupswdmap.insertsu_pswd(stupswdi);
        session.commit();
        session.close();
        return insnum;
    }
}
